const express = require("express");
const app = express();
const PORT = 3000;
const bodyParser = require("body-parser")
require('dotenv').config();

const nodemailer = require('nodemailer')


let transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: process.env.EMAIL,
        pass: process.env.PASSWORD
    }
})

app.use(express.static('public'))

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.post('/sent', function(req, res){
    
    // enviar email separa as variaveis de post
    sname=req.body.name
    semail=req.body.email //saber qm responder
    smeutexto=req.body.meutexto
 
    //step 2
    let mailOptions = {
    from: 'camilatestemails@gmail.com', //.env
    to:  `camilaalmeida@infojr.com.br`, // email que eu recebo os emails
    cc: `${semail}`,
    subject: `Email de ${sname} `,
    text: `${smeutexto}`
    }
    
    console.log(mailOptions)


    //step 3
    transporter.sendMail(mailOptions, function(err, data){
    if (err){
        console.log('error');
    } else{
        console.log('email sent')
    }
    });
    


    // voltar pro HTML (obrigado)
    res.sendFile(__dirname+'/public/thanks.html')
    
})


app.get("/redirect", async (req, res) => {
    res.redirect("/")
  })
  



app.listen(PORT, () => {
    console.log(`Running in http://localhost:${PORT}`);
})
