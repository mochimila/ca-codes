module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
        fontSize: {
          sm: ['16px', '20px'],
          base: ['20px', '30px'],
          lg: ['32px', '38px'],
          xl: ['40px', '54px'],
        },
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
