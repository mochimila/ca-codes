

document.getElementById('site').innerHTML = 
`  


<header>
<nav class="fixed w-screen bg-white md:px-10 px-8 md:flex justify-between items-center border-gray-600">
    <div class="flex px-4 justify-around my-2 md:justify-between items-center md:py-0 pb-1">
        <a href="">
            <img src="images/Framelogo.svg" alt="Logo"
            class=''>
        </a>
        

        <div class="menu-btn md:hidden ml-10" id='navIcon'> 
            <div class="menu-btn__burger"></div>                    
        </div>
    </div>
    
    <ul class='hidden-menu md:my-4 hidden md:flex cursor-pointer text-center' id='navContent'>            
        <li class='navfonta font-medium text-4xl md:text-sm text-black py-10 pt-5 md:py-2 px-6  transition hover:bg-gray-400 duration-200 '><a href="">Features</a></li>
        <li class='navfonta font-medium text-4xl md:text-sm text-black py-10 pt-5 md:py-2 px-6  transition hover:bg-gray-400 duration-200 '><a href="">Services</a></li>
        <li class='navfonta font-medium text-4xl md:text-sm text-black py-10 pt-5 md:py-2 px-6 md:mr-1 transition hover:bg-gray-400 duration-200 '><a href="">Contact</a></li>
        <button class="navfontb font-medium text-4xl md:text-sm md:border py-10 pt-5 md:py-2 px-6 outline-none " >Get a demo</button>
    </ul>
</nav>
</header>

    <main class="  flex flex-col py-24 ">    

        <div class="div1 flex flex-col px-4 py-12 md:pb-0 md:pt-40">
            <div class="">
                <p class="text-center text-white font-bold text-4xl mb-10 md:text-5xl">Find the right partners to <br class="hidden md:block">
                    fuel your business growth</p>
                <p class="text-center text-white font-normal my-10">Join a vibrant community of MSPs to forge long-lasting relationships with <br class="hidden md:block"> partners that help you create excellent customer experiences</p>
            </div>

            <div class="flex justify-center md:justify-around">
                <img class="hidden md:inline-flex" src="images/PartnerHeroImageLeft.svg">
                <button class="div1b font-bold text-white bg-black py-4 px-8 md:my-20">Register today</button>
                <img class="hidden md:inline-flex" src="images/PartnerHeroImageRight.svg">
            </div>
        </div>


        <div class="div2 items-center flex flex-col md:pt-8 md:pb-16">
            <div>
                <p class="text-4xl font-medium pt-12 pb-2 md:tamfon">List. Sell. Grow</p>
            </div>

            <div class="md:flex md:flex md:justify-center">                
                <div class="items-center flex flex-col my-8 mx-16 md:w-1/6 max-w-xs">
                    <img class="pb-6" src="images/icon1sqr.svg">
                    <p class="text-center ">Be a part of the only  All-In-One platform for IT</p>
                </div>

                <div class="items-center flex flex-col my-8 mx-16 md:w-1/6  max-w-xs">
                    <img class="pb-6" src="images/icon2mg.svg">
                    <p class="text-center">Come closer to finding your next customer</p>
                </div>

                <div class=" items-center flex flex-col my-8 mx-16 md:w-1/6 max-w-xs">
                    <img class="pb-6" src="images/icon3gr.svg">
                    <p class="text-center">Grow your business with a single click</p>
                </div>

            </div>

        </div>

        <div class="mx-6 my-4">

        
            <div class="divs flex flex-col items-center text-center py-16 md:flex-row-reverse md:justify-evenly">

                    <img class="md:pr-10 md:items-center md:h-full" src="images/figure1man.svg">
    
                <div class="flex flex-col items-center mt-6 md:mt-0 0 md:w-1/3 md:text-left md:items-start">
                    <p class="titledivs font-bold my-6 ">Your channel program 
                        on steroids</p>
                    <p class="md:text-left w-11/12"">Tired of finding MSPs that can help you boost your channel sales?  You’re in luck, because they’re all here.</p>
                </div>
            </div>

            <div class="divs flex flex-col items-center text-center py-16 md:flex-row md:justify-evenly">
    
                    <img class=" md:pl-10 md:items-center md:h-full" src="images/figure2man+plant.svg">
    
                <div class="flex flex-col items-center mt-6 md:mt-0 0 md:w-1/3 md:text-left md:items-start">
                    <p class="titledivs font-bold my-6">Become the vendor
                        everyone wants to buy from</p>
                    <p class="md:text-left w-11/12" >Gain brand authority and visibility with the help of the largest IT service ecosystem. Find customers who are looking for your solution right now!</p>
                    
                    <div class="flex md:justify-self-start">
                        <button class="flex-1 flex justify-center mt-6 md:justify-items-start font-medium">Singup now<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M14 5l7 7m0 0l-7 7m7-7H3" />
                        </svg> </button>
                    </div>
                </div>
            </div>

            <div class="divs flex flex-col items-center text-center py-16 md:px-20  md:flex-row-reverse md:justify-evenly ">
 
                        <img class=" md:pr-10 md:items-center md:h-full"   src="images/figure3man+rocket.svg">
    
    
                <div class="flex flex-col items-center mt-6 md:mt-0 0 md:w-1/3 md:text-left md:items-start ">
                    <p class="titledivs font-bold my-6">It’s like having Your best Selesman on autopilot </p>
                    <p class="md:text-left w-11/12"> Share collaterals and documents that are automatically branded. Leverage a rich partner network that sells your solution exactly the way your best salesman would.</p>
                    
                    <div class="flex md:justify-self-start">
                        <button class="flex-1 flex justify-center mt-6 md:justify-items-start font-medium">Singup now<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M14 5l7 7m0 0l-7 7m7-7H3" />
                        </svg> </button>
                    </div>    
                </div>
            </div>

            <div class="divs flex flex-col items-center text-center py-16 md:flex-row md:justify-evenly">

                <img class=" md:pl-10 md:items-center md:h-full" src="images/figure4table.svg">

                <div class="flex flex-col items-center mt-6 md:mt-0 0 md:w-1/3 md:text-left md:items-start ">
                    <p class="titledivs font-bold my-6" >Track your channel performance</p>
                    <p class="md:text-left w-11/12" > Monitor the health and revenue of your channel program and streamline your campaign to win more customers within Zomentum. </p>
                    
                    <div class="flex md:justify-self-start">
                        <button class="flex-1 flex justify-center mt-6 md:justify-items-start font-medium">Singup now<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M14 5l7 7m0 0l-7 7m7-7H3" />
                        </svg> </button>
                    </div>
                </div>
            </div>

        </div>

        <div class="flex flex-col divf bg-black px-12 py-12 gap-10">
            <div class="flex justify-around ">                
                <div class="w-2/4">
                    <ul>
                        <li class="text-white font-bold mb-4">Product
                            <li class="text-white text-sm mb-2">Features</li>
                            <li class="text-white text-sm mb-2">Pricing</li>
                            <li class="text-white text-sm mb-2">Integrations</li>
                            <li class="text-white text-sm mb-2">Product</li>
                        </li>
                    </ul>

                </div>

                <div>
                    <ul>
                        <li class="text-white font-bold mb-4">Company
                            <li class="text-white text-sm mb-2">About us</li>
                            <li class="text-white text-sm mb-2">Contact us</li>
                            <li class="text-white text-sm mb-2">Submit a ticket</li>
                            <li class="text-white text-sm mb-2">Privacy policy</li>
                            <li class="text-white text-sm mb-2">Terms & conditions</li>
                        </li>
                    </ul>

                </div>
            </div>

            <div  class="flex justify-around"> 
                <div class="w-2/4">
                    <ul>
                        <li class="text-white font-bold mb-4">Guides
                            <li class="text-white text-sm mb-2">MSP</li>
                            <li class="text-white text-sm mb-2">MSP Sales</li>

                        </li>
                    </ul>

                </div>
                
                <div class="w-2/4">
                    <ul>
                        <li class="text-white font-bold mb-4 mr-4">Contact Us
                            <li class="text-white text-sm mb-2">Adress</li>
                        </li>
                    </ul>

                </div>
            </div>

            <div class="flex md:justify-center gap-2">
                <p class="text-white text-center text-sm">© </p>
                <p class="text-white text-center text-sm" id="demo"></p>
                <p class="text-white text-center text-sm">  Pactora Inc. All rights reserved.</p>
            </div>

            
        </div>






    </main>
    

`;


$(document).ready(function() {
  $('#navIcon').click(function() {
    $('#navContent').toggleClass('block').toggleClass('hidden');
  })
})


const menuBtn = document.querySelector('.menu-btn');
let menuOpen = false;
menuBtn.addEventListener('click', () => {
if(!menuOpen) {
  menuBtn.classList.add('open');
  menuOpen = true;
} else {
  menuBtn.classList.remove('open');
  menuOpen = false;
}
});

  