//creating docs
function createDoc(){
  var doc = DocumentApp.create('new test doc')
}

//accessing docs
function myFunction() {
  var doc = DocumentApp.getActiveDocument();
  Logger.log(doc.getId());
  Logger.log(doc.getUrl());
}

//adding things to doc
function updateDoc(){
  var doc = DocumentApp.openById('1EjEihXN4cyHq45OcS6RORxcpOkqy0GXr9f3veIswTVI');
//  var doc = DocumentApp.openByUrl('https://docs.google.com/open?id=1EjEihXN4cyHq45OcS6RORxcpOkqy0GXr9f3veIswTVI');
  var body = doc.getBody();
  body.appendParagraph('new content ' + Date());
  body.appendHorizontalRule();
  body.appendPageBreak();
  Logger.log(body);

  Logger.log(doc.getName());

}

//changing language
function seeDoc(){
  var body = DocumentApp.openById('1EjEihXN4cyHq45OcS6RORxcpOkqy0GXr9f3veIswTVI');
  var selection = body.getText();
  Logger.log(selection);
  var spanish = LanguageApp.translate(selection, 'en', 'es');
  Logger.log(spanish);
  body.appendParagraph(spanish);

}


//accessing paragraphs, changing font, size, colors, etc.
function seeParagraph(){
  var body = DocumentApp.openById('1EjEihXN4cyHq45OcS6RORxcpOkqy0GXr9f3veIswTVI');
  var selection = body.getText();
  var p1 = body.getChild(2);
  Logger.log(p1.getText());
  p1.setText('CHANGED!');
  p1.setFontSize(50);

  var paraList = body.getParagraphs();
  Logger.log(paraList[2].getText());
  paraList[3].appendText('ADDED');
  var a = paraList[3].getAttributes();
  Logger.log(a);
  
  var style = {};
  style[DocumentApp.Attribute.HORIZONTAL_ALIGNMENT] = DocumentApp.HorizontalAlignment.RIGHT;
  style[DocumentApp.Attribute.FONT_FAMILY] = 'Calibri';
  style[DocumentApp.Attribute.BOLD] = true;
  style[DocumentApp.Attribute.FOREGROUND_COLOR] = '#fff090';
  style[DocumentApp.Attribute.BACKGROUND_COLOR] = '#FFC0CB';
  paraList[2].setAttributes(style);

}


  //types of attributes
  /* {LINK_URL=null, BOLD=null, ITALIC=null, FOREGROUND_COLOR=null, FONT_FAMILY=null, UNDERLINE=null, 
    LINE_SPACING=1.15, FONT_SIZE=null, SPACING_AFTER=0.0, SPACING_BEFORE=0.0, HORIZONTAL_ALIGNMENT=Left,
    INDENT_START=0.0, LEFT_TO_RIGHT=true, BACKGROUND_COLOR=null, HEADING=Normal, STRIKETHROUGH=null, 
    INDENT_END=0.0, INDENT_FIRST_LINE=0.0} */


//paragraphs: replace and lenght
function seeParagraphTwo(){
  var body = DocumentApp.openById('1EjEihXN4cyHq45OcS6RORxcpOkqy0GXr9f3veIswTVI');
  Logger.log(body.getParagraphs().length);
  //Logger.log(body.getNumChildren());
  body.replaceText('content', 'NEW UPDATED STRING')


  for(var x=0; x < body.getNumChildren(); x++){
    var el = body.getChild(x);
    if (el.getType() == 'PARAGRAPH'){
      el.appendText('====' + el.getText().length+'=====')
      Logger.log(el.getText().length)
    }
  }
}

//paragraphs: attribute changes
function seeParagraphThree(){
  var doc = DocumentApp.openById('1EjEihXN4cyHq45OcS6RORxcpOkqy0GXr9f3veIswTVI');
  var body = doc.getBody();
  var att = {
    "FOREGROUND_COLOR" : "#FFFF00",
    "BOLD" : true
  }
  for(var x=0; x < body.getNumChildren(); x++){
    var el = body.getChild(x);
    //el.setAttributes(att);
    var text = el.editAsText();
    Logger.log(text.length);
    text.setForegroundColor(0, (text.getText().length/2), '#FF0000');
    text.setBackgroundColor((text.getText().length/2), text.getText().length-1, '#00FF00');
  }
}

//copying docs
function copyDoc(){
  var srcDoc = DocumentApp.openById('1EjEihXN4cyHq45OcS6RORxcpOkqy0GXr9f3veIswTVI');
  var tarDoc = DocumentApp.openById('1LFTdlJ7J_QVVKxRpFCEw65mVGbu3qotcB0pB2gwgkVQ');
  var srcDocTotal = srcDoc.getNumChildren();
  var tarDocBody = tarDoc.getBody();
  tarDocBody.clear();
  tarDocBody.appendParagraph('Last updated ' + Date()).setFontSize(8).appendHorizontalRule();
  
  for(var x=0; x < srcDocTotal; x++){
    var el = srcDoc.getChild(x).copy();
    var elType = el.getType();

    if(elType == DocumentApp.ElementType.PARAGRAPH){
      tarDocBody.appendParagraph(el);
    }
    else if(elType == DocumentApp.ElementType.LIST_ITEM){
      tarDocBody.appendListItem(el);
    }
    else if(elType == DocumentApp.ElementType.TABLE){
      tarDocBody.appendTable(el);
    }
    Logger.log(el);
  }

}